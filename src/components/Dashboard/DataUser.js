import React, { useContext } from 'react';
import styled from 'styled-components';

import { UserContext } from '../../context/UserContext';

const ContainerUser = styled.fieldset`
    border-radius: 5px;
    border: 1px groove #77c9d4;
    box-shadow: -2px 2px 4px #77c9d4;
    color: #a5a5af;
`;

const DataUser = () => {

    const { userLogged } = useContext(UserContext);

    return (
        <ContainerUser
            className="mt-2"
        >
            <legend
                className="text-semibold"
            >{userLogged.name} {userLogged.surname}</legend>
            <div className="row">
                <div className="col-6 col-md-4">
                    <p><b>{userLogged.balance_USD}</b> USD</p>
                </div>
                <div className="col-6 col-md-4">
                    <p><b>{userLogged.balance_BTC}</b> BTC</p>
                </div>
                <div className="col-12 col-md-4">
                    <p><b>email:</b> {userLogged.email}</p>
                </div>
            </div>
        </ContainerUser>
    );
};

export default DataUser;