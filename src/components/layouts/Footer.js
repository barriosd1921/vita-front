import React from 'react';
import styled from 'styled-components';

const Foot = styled.footer`
    padding: 0 20px;
    background-color: #57bc90;
    height: 40px;
    
    p {
        text-align: center;
        color: #FFF;
        margin: 8px 0;

        a {
            color: #FFF;
        }
    }
`;

const Footer = () => {
    return (
        <Foot
            className="d-flex justify-content-center align-items-center"
        >
            <p>
                Realizador por: <a 
                    href="http://iuwebzone.com/t/desarrollador/Davidbarrios" 
                    target="_blank" 
                    rel="noopener noreferrer"
                >David Barrios</a>
            </p>
        </Foot>
    );
};

export default Footer;