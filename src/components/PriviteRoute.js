import React, { useContext } from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

import { UserContext } from '../context/UserContext';

const PriviteRoute = ({ component: Component, ...props}) => {

    const { validateSession } = useContext(UserContext);

    return (
        <Route {...props}
            render={ props =>  !validateSession() ? (
                props.location.pathname === '/login' || props.location.pathname === '/register' ? <Component {...props} /> : <Redirect to="/login" />
            ) : (
                props.location.pathname === '/login' || props.location.pathname === '/register' ? <Redirect to="/dashboard" /> : <Component {...props} />
            )}
        />
    );
};

PriviteRoute.propTypes = {
    Component: PropTypes.element
}

export default PriviteRoute;