import React, { Fragment, useEffect, useState, useContext } from 'react';
import styled from 'styled-components';
import dateFormat from 'dateformat';
import PropTypes from 'prop-types';

// Importando componentes
import Exchange from '../Dashboard/Exchange';
import ToDashboard from '../../helpers/ToDashboard';

import { TransactionContext } from '../../context/TransactionContext';

import { breaks } from '../../helpers/constants';

const Container = styled.div`
    min-height: calc(100vh - 100px);
    overflow: hidden;
    color: #a5a5af;

    > .row {
        border: 1px solid #57bc90;
        border-radius: 5px;
    }

    legend {
        color: #57bc90;
        font-weight: 600;
    }

    ${breaks} {
        
    }
`;

const ContainerAlert = styled.div`
    width: 100%;
    margin: 20px;
    box-shadow: -2px 2px 4px#57bc90;
    border-radius: 5px;
    border: 1px solid#57bc90;
    color: #a5a5af;
`;

const Details = ({match}) => {

    const [transaction, setTransaction ] = useState(null);

    const { getTransactionById } = useContext(TransactionContext);

    useEffect( () => {
        getTransactionById(match.params.id)
            .then(
                response => {
                    if(response.status === 'success') {
                        setTransaction(response.transaction);
                    } else {
                        console.error("Error en consulta de transaccion por id -> ",response);
                    }
                }
            );
    },[]);

    return (
        <Fragment>
            <Container
                className="container px-2"
            >   
                <Exchange />
                <div className="row mt-2 py-2">
                    <div className="col-12">
                        <h3
                            className="border-bottom-1 pb-1 my-0"
                        >Detalles de transacción: <i>{match.params.id}</i></h3>
                    </div>
                    {
                        !transaction ? 
                        <ContainerAlert
                            className="mt-2"
                        >
                            <h2
                                className="text-center"
                            >
                                No se encontro información de la transacción
                            </h2>
                        </ContainerAlert>
                        :
                        (
                            <Fragment>
                                <div className="col-12 col-md-4 mt-1">
                                    <fieldset>
                                        <legend>Fecha y Hora</legend>
                                        {dateFormat(transaction.created_at, "dd/mm/yyyy h:MM TT")}
                                    </fieldset>
                                </div>
                                <div className="col-12 col-md-4 mt-1">
                                    <fieldset>
                                        <legend>Moneda Enviada</legend>
                                        {transaction.currency_send}
                                    </fieldset>
                                </div>
                                <div className="col-12 col-md-4 mt-1">
                                    <fieldset>
                                        <legend>Moneda Recibida</legend>
                                        {transaction.currency_receiver}
                                    </fieldset>
                                </div>
                                <div className="col-12 col-md-4 mt-1">
                                    <fieldset>
                                        <legend>Tasa de Cambio</legend>
                                        {
                                            transaction.currency_send === 'USD' ?
                                                `1 BTC = ${transaction.rate_exchange.toFixed(2)} USD` 
                                                :
                                                `${transaction.rate_exchange.toFixed(2)}USD = 1BTC`
                                        }
                                    </fieldset>
                                </div>
                                <div className="col-12 col-md-4 mt-1">
                                    <fieldset>
                                        <legend>Monto enviado</legend>
                                        {transaction.amount_send}
                                    </fieldset>
                                </div>
                                <div className="col-12 col-md-4 mt-1">
                                    <fieldset>
                                        <legend>Monto recibido</legend>
                                        {transaction.amount_received}
                                    </fieldset>
                                </div>
                            </Fragment>
                        )
                    }
                </div>
                
            </Container>
            <ToDashboard />
        </Fragment>
    );
};

Details.propTypes = {
    match: PropTypes.object
}

export default Details;