import React, { Fragment} from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';

import back_arrow from '../assets/back_arrow.png';

const ContainerBack = styled.div`
    position: absolute;
    top: 5%;
    left: 0;
    border-radius: 0 10px 10px 0;
    background-color: rgb(255,255,255);
    width: 40px;
    height: 40px;
    display: flex;
    align-items: center;
    justify-content: center;
    box-shadow: 1px 1px 5px rgba(0,0,0,.3);
    cursor: pointer;
    
    img {
        width: 60%;
    }
`;

const ToDashboard = ({history}) => {

    const goHome = () => {
        history.push('/dashboard');
    }

    return (
        <Fragment>
            <ContainerBack onClick={ () => goHome()}>
                <img src={back_arrow} alt="Icon back"/>
            </ContainerBack>
        </Fragment>
    );
};

ToDashboard.propTypes = {
    history: PropTypes.object
}

export default withRouter(ToDashboard);