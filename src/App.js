import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';

// Importando Componentes
import Header from './components/layouts/Header';
import Footer from './components/layouts/Footer';
import Login from './components/Login/Login';
import Register from './components/Registers/Register';
import Dashboard from './components/Dashboard/Dashboard';
import Details from './components/Details/Details';

// Importando Context 
import UserProvider from './context/UserContext';
import ExchangeProvider from './context/ExchangeContext';
import TransactionProvider from './context/TransactionContext';

// Importando componente para proteccion de rutas
import PriviteRoute from './components/PriviteRoute';

function App() {

  return (
    <Router>
      <UserProvider>
        <ExchangeProvider>
          <TransactionProvider>
            <Header />
            <Switch>
              <PriviteRoute exact path="/login"
                component={Login}
              />

              <PriviteRoute exact path="/register"
                component={Register}
              />

              <PriviteRoute exact path="/dashboard"
                component={Dashboard}
              />

              <PriviteRoute exact path="/details/:id"
                component={Details}
              />
              <Route 
                render={ () => (
                  <Redirect to="/login"/>
                )}
              />
            </Switch>
            <Footer />
          </TransactionProvider>
        </ExchangeProvider>
      </UserProvider>
    </Router>
  );
}

export default App;
