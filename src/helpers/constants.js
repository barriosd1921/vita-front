const environment = 'prod';

const url_list = {
    dev: 'http://127.0.0.1:3000/api/v1',
    prod: 'https://vita-api-dbarrios.herokuapp.com/api/v1'
};

export const url_base = url_list[environment];

export const breaks = `@media screen and (max-width: 520px)`;

export const modalStyles = `
    width: 100vw;
    height: 100vh;
    background-color: rgba(0,0,0,.7);
    position: fixed;
    top: 0;
    left: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    z-index: 99999;
    animation-duration: 1s;
    animation-name: ModalIn;

    @keyframes ModalIn {
        0% {
            opacity: 0;
        }
        100% {
            opacity: 1;
        }
    }
`;
