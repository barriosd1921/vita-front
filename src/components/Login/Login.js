import React, { useState, useContext } from 'react';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { UserContext } from '../../context/UserContext';

import styled from 'styled-components';
import BG_MV from '../../assets/BG_MV.jpg';
import BG_WB from '../../assets/BG_WB.jpg';

import { breaks } from '../../helpers/constants';

const Container = styled.div`
    background: url(${BG_WB}) no-repeat top left/cover;
    height: calc(100vh - 100px);

    ::before {
        content: '\\A';
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background-color: rgba(87, 188, 144,.5);
        z-index: 0;
    }

    ${breaks} {
        background: url(${BG_MV}) no-repeat top left/cover;
    }
`;

const Box = styled.div`
    background-color: #FFF;
    position: absolute;
    right: 50px;
    top: 50%;
    transform: translate(0,-50%);
    border-radius: 5px;
    box-shadow: -2px 2px 4px #77c9d4;
    width: 450px;

    h1 {
        color: #a5a5af;
    }

    ${breaks} {
        width: 90%;
        right: 50%;
        transform: translate(50%,-50%);
    }
`;

const Form = styled.form`
    margin: 20px;
    
    input {
        margin-bottom: 10px;
    }

    a {
        color: #a5a5af;
        text-decoration: none;
    }

    button {
        display: block;
        width: -webkit-fill-available;
    }
`;

const Login = ({history}) => {

    // Creando state local para login 
    const [data, setData] = useState({
        email: '',
        password: ''
    });

    // Exportando context de user para login 
    const { Login } = useContext(UserContext);

    const handleInput = e => {
        setData({
            ...data,
            [e.target.name] : e.target.value
        });
    }

    const handleSubmit = e => {
        e.preventDefault();

        Login(data);
    }

    return (
        <Container>
            <Box>
                <h1
                    className="text-center my-1"
                >Iniciar sesión</h1>
                <Form
                    onSubmit={handleSubmit}
                >
                    <input 
                        type="email"
                        name="email"
                        className="form-control"
                        onChange={handleInput}
                        required
                        placeholder="Correo electrónico"
                    />
                    <input 
                        type="password" 
                        name="password"
                        className="form-control"
                        onChange={handleInput}
                        required
                        placeholder="Contraseña"
                    />
                    <Link 
                        to="/register"
                        className="my-2 d-block"
                    >¿No tienes cuenta? Registrate</Link>
                    <button
                        type="submit"
                        className="btn btn-primary mt-1"
                    >Iniciar Sesión</button>
                </Form>
            </Box>
        </Container>
    );
};

Login.propTypes = {
    history: PropTypes.object
}

export default withRouter(Login);