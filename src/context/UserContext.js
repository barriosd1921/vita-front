import React, { useState, createContext, useEffect } from 'react';
import { withRouter } from "react-router-dom";
import axios from 'axios';
import Swal from 'sweetalert2';

import { url_base } from '../helpers/constants';

export const UserContext = createContext();

const UserProvider = props => {

    const [userLogged, setUserLogged] = useState({});
    const [actionLogin, setActionLogin ] = useState(false);

    const Login = async data => {
        
        await axios.post(
            `${url_base}/user/login`, 
            {user: JSON.stringify(data)}
        )
        .then( response => {
            if(response.data.status === 'success'){

                setUserLogged(response.data.user);
                localStorage.setItem('userLogged',JSON.stringify(response.data.user));

                Swal.fire({
                    title: 'Bienvenido',
                    text: 'Disfruta de Vita Test DB',
                    icon: 'success',
                    showConfirmButton: false,
                    timerProgressBar: true,
                    timer: 2000
                })
                .then( () => {
                    setActionLogin(true);
                    props.history.push('/dashboard');
                });
            } else {
                Swal.fire({
                    title: 'ERROR',
                    text: 'Credenciales incorrectas',
                    icon: 'error',
                    showConfirmButton: false,
                    timerProgressBar: true,
                    timer: 2500
                });
            }
        })
        .catch( e => {
                console.error(e);
                Swal.fire({
                    title: 'ERROR',
                    text: 'Lo sentimos hay un error inesperado en nuestro sistema',
                    icon: 'error',
                    showConfirmButton: false,
                    timerProgressBar: true,
                    timer: 3000
                })
            }
        );
    }

    const createUser = async data => {
        const request = await axios.post(
            `${url_base}/user/register`,
            { user: JSON.stringify(data)});
        
        return request.data;
    }

    const updateSession = async () => {
        await axios.get(
            `${url_base}/user/updateSession`,
            { headers: { Authorization: `Bearer ${userLogged.token}` } }
        )
        .then( response => {            
            if(response.data.status === 'success'){    
                setUserLogged(response.data.user);
                localStorage.setItem('userLogged',JSON.stringify(response.data.user));
            } else {
                console.error("Error al actualizar sesion -> ", response);
            }
        })
    }

    const Logout = () => {
        localStorage.removeItem('userLogged');
        props.history.push('/login');
    }

    const validateSession = () => {
        return localStorage.getItem('userLogged') ? true : false;
    }

    const getToken = () => ( JSON.parse(localStorage.getItem('userLogged')) ? JSON.parse(localStorage.getItem('userLogged'))['token'] : null)

    useEffect( () => {
        setUserLogged(JSON.parse(localStorage.getItem('userLogged')) ? JSON.parse(localStorage.getItem('userLogged')) : {})
    },[]);

    return (
        <UserContext.Provider
            value={{
                userLogged,
                actionLogin,
                Login,
                Logout,
                getToken,
                validateSession,
                updateSession,
                createUser
            }}
        >
            {props.children}
        </UserContext.Provider>
    );
};

export default withRouter(UserProvider);