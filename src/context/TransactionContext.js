import React, { createContext, useContext} from 'react';
import axios from 'axios';

import { url_base } from '../helpers/constants';
import { UserContext } from './UserContext';

export const TransactionContext = createContext();

const TransactionProvider = (props) => {

    const { getToken } = useContext(UserContext);

    const getTransactionsByUser = async () => {

        const transactions = await axios.get(
            `${url_base}/transaction/getTransactionsByUser`,
            { headers: { Authorization: `Bearer ${getToken()}` } }
        );

        return transactions.data;
    }

    const createTransaction = async data => {
        const request = await axios.put(
            `${url_base}/transaction/setTransaction`,
            { data : JSON.stringify(data) },
            { headers: { Authorization: `Bearer ${getToken()}` } }
        );

        return request.data;
    }

    const getTransactionById = async id => {
        const request = await axios.get(
            `${url_base}/transaction/getTransactionById/${id}`,
            { headers: { Authorization: `Bearer ${getToken()}` } }
        );

        return request.data;
    }

    return (
        <TransactionContext.Provider
            value={{
                getTransactionsByUser,
                createTransaction,
                getTransactionById
            }}
        >
            {props.children}
        </TransactionContext.Provider>
    );
};

export default TransactionProvider;