import React, { useState, Fragment, useContext } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

// Importando context customs
import { ExchangeContext } from '../../context/ExchangeContext';
import { UserContext } from '../../context/UserContext';

// Importando componentes 
import ModalRequest from '../layouts/ModalRequest';

import { breaks } from '../../helpers/constants';

import arrow from '../../assets/exchange_arrow.png';

const Container = styled.div`
    box-shadow: -2px 2px 4px #77c9d4;
    border-radius: 5px;
    border: 1px solid #77c9d4;
    color: #a5a5af;

    ${breaks} {
        flex-direction: column;
    }
`;

const ContainerExchange = styled.div`
     h3 {
        text-transform: uppercase;
        font-weight: 600;
    }

    img {
        width: 18px;
        vertical-align: middle;
    }

    > div * {
        transition: order 1s;
    }

    ${breaks} {
        flex-direction: column;

        h3 {
            margin: 0 !important;
        }

        > div {
            margin-top: 10px;
        }
    }
`;

const ContainerButton = styled.div`
    margin-left: auto;

    button {
        background-color: #a5a5af;
        border: 1px solid #57bc90;
        color: #FFF;
        font-weight: 400;
    }

    ${breaks} {
        margin-left: 0;
        margin-top: 20px;
        width: 100%;
        
        button {
            width: 100%;
        }
    }
`;

const Exchange = ({isRefresh}) => {
    
    const [modal,setModal] = useState(false);    

    const { exchange } = useContext(ExchangeContext);
    const { userLogged } = useContext(UserContext);

    const [order, setOrder] = useState({
        first: {
            order: 1
        },
        second: {
            order: 2
        },
        thrid: {
            order: 3
        }
    });

    const changeOrder = () => {
        setOrder({
            ...order,
            first: {
                order: order.first.order === 1 ? 3 : 1
            },
            thrid: {
                order: order.thrid.order === 1 ? 3 : 1
            }
        });
    }

    return (
        <Fragment>
            <Container
                className="d-flex align-items-center mt-2 py-2 px-2"
            >
                <ContainerExchange
                    className="d-flex align-items-center"
                >
                    <h3
                        className="mr-2 my-0"
                    >Tasa de cambio actual</h3>
                    <div
                        className="d-flex"
                    >
                        <label
                            style={order.first}
                        >
                            <b>{order.first.order === 1 ? 1 : exchange.rate_sell ? (1/exchange.rate_sell).toFixed(8) :  null}</b> BTC
                        </label>
                        <img 
                            src={arrow} 
                            alt="Change" 
                            title="Cambiar" 
                            style={order.second}
                            onClick={changeOrder}
                            className="clickable mx-1"
                        />
                        <label
                            style={order.thrid}
                        >
                            <b>{order.thrid.order === 1 ? 1 : exchange.rate_buy ? exchange.rate_buy.toFixed(2) : null}</b> USD
                        </label>
                    </div>
                </ContainerExchange>
                <ContainerButton>
                    <button
                        className="btn"
                        onClick={() => setModal(true)}
                    >
                        Nueva Transacción
                    </button>
                </ContainerButton>
            </Container>
            {
                modal ? 
                    <ModalRequest 
                        action={setModal}
                        exchange={exchange}
                        userLogged={userLogged}
                        isRefresh={isRefresh}
                    /> 
                    : 
                    null
            }
            
        </Fragment>
    );
};

Exchange.propTypes = {
    isRefresh: PropTypes.func
}

export default Exchange;