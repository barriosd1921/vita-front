import React, { useState, useEffect, createContext, useContext } from 'react';
import { withRouter } from "react-router-dom";
import axios from 'axios';

import { url_base } from '../helpers/constants';
import { UserContext } from './UserContext';

export const ExchangeContext = createContext();

const ExchangeProvider = props => {

    const [exchange, setExchange] = useState({});

    const { getToken, actionLogin } = useContext(UserContext);

    useEffect( () => {
        let timer;

        if(!getToken()) return;

        const getExchange = async () => {
            const response = await axios.get(
                `${url_base}/exchange/getExchange`,
                { headers: { Authorization: `Bearer ${getToken()}` } }
            );
            
            if(response.data.status === 'success'){
                const data = response.data.exchange;
    
                setExchange({
                    rate_sell: parseFloat(data.rate)*(1+(data.percent_sell/100)),
                    rate_buy: parseFloat(data.rate)*(1-(data.percent_buy/100))
                });
    
            } else {
                console.error("Error inesperado al consultar tasa de cambio");
            }    
        }

        const startCount = () => {
            let time_max = 180
            timer = setInterval(() => {
                time_max--;

                if (time_max <= 0) {
                    getExchange();
                    clearInterval(timer);
                }
            }, 1000);
        }

        if(Object.keys(exchange).length <= 0)
            getExchange();
        else
            startCount();
    },[exchange, actionLogin]);

    return (
        <ExchangeContext.Provider
            value={{
                exchange,
                setExchange
            }}
        >
            {props.children}
        </ExchangeContext.Provider>
    );
};

export default withRouter(ExchangeProvider);