import React, { useContext } from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import { UserContext } from '../../context/UserContext';

import icon_logout from '../../assets/icon_logout.png';

import { breaks } from '../../helpers/constants';

const Head = styled.header`
    position: fixed;
    z-index: 999;
    left: 0;
    right: 0;
    top: 0;
`;

const Nav = styled.nav`
    padding: 0 40px;
    height: 60px;
    background: linear-gradient(to right, #77c9d4 0, #77c9d4 20%, #57bc90 51%, #57bc90 100%);

    a {
        color: #FFF;
        text-decoration: none;
        font-size: 2em;
    }

    button {
        background-color: transparent;
        border: none;
        margin-left: auto;

        &:focus{
            outline: none;
        }

        img {
            width: 30px;
        }
    }

    ${breaks} {
        padding: 0 15px;
    }
`;

const Header = () => {

    const { Logout, validateSession } = useContext(UserContext);

    return (
        <Head>
            <Nav
                className="d-flex align-items-center"
            >
                <Link to="/">Vita Test DB</Link>
                {
                    validateSession() ? <button
                        type="button"
                        onClick={Logout}
                    >
                        <img 
                            src={icon_logout} 
                            alt="Logout"
                        />
                    </button> : null
                }
            </Nav>
        </Head>
    );
};

export default Header;