import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import dateFormat from 'dateformat';

const Transaction = ({item}) => {

    return (
        <tr>
            <td>
                {dateFormat(item.created_at, "dd/mm/yyyy h:MM TT")}
            </td>
            <td>
                {item.currency_receiver}
            </td>                        
            <td>
                {item.amount_received}
            </td>
            <td>
                {
                    item.currency_receiver === 'BTC' ?
                        `1BTC = ${item.rate_exchange.toFixed(2)} USD` 
                        :
                        `1USD = ${(1/item.rate_exchange).toFixed(8)} BTC`
                }
            </td>
            <td>
                <Link to={`/details/${item.id}`}>
                    Detalles
                </Link>
            </td>
        </tr>
    );
};

Transaction.propTypes = {
    item: PropTypes.object.isRequired
}

export default Transaction;