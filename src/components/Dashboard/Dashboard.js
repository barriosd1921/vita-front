import React, { useState } from 'react';
import styled from 'styled-components';

// Importando componentes
import Exchange from './Exchange';
import Transactions from './Transactions';
import DataUser from './DataUser';

import { breaks } from '../../helpers/constants';

const Container = styled.div`
    min-height: calc(100vh - 100px);
    overflow: hidden;

    ${breaks} {
        
    }
`;

const Dashboard = () => {

    const [isRefresh, setIsRefresh] = useState(false);

    return (
        <Container
            className="container px-2"
        >
            <Exchange 
                isRefresh={setIsRefresh}
            />
            <DataUser />
            <Transactions 
                refresh={isRefresh}
            />
        </Container>
    );
};

export default Dashboard;