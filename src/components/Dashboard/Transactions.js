import React, { useState, useEffect, useContext, Fragment } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import Transaction from './Transaction';

import { TransactionContext } from '../../context/TransactionContext';

const ContainerTable = styled.div`
    display: block;
    width: 100%;
    overflow-x: auto;
    border: 1px solid #57bc90;
    border-radius: 5px;
`;

const ContainerAlert = styled.div`
    box-shadow: -2px 2px 4px#57bc90;
    border-radius: 5px;
    border: 1px solid#57bc90;
    color: #a5a5af;
`;

const Transactions = ({refresh}) => {

    const { getTransactionsByUser } = useContext(TransactionContext);

    const [list, setList] = useState([]);

    useEffect( ()=>{
        getTransactionsByUser().then(
            response => {
                if(response.status === 'success')
                    setList(response.transactions);
                else
                    console.error("ERROR al consultar transacciones -> ", response);
            }
        );
    },[refresh] );

    return (
        <Fragment>
            {
                list.length <= 0 ?
                    <ContainerAlert
                        className="mt-2"
                    >
                        <h2
                            className="text-center"
                        >
                            No has realizado transacciones hasta la fecha
                        </h2>
                    </ContainerAlert>
                    :
                    <ContainerTable
                        className="my-2"
                    >
                        <table
                            className="table"
                        >
                            <thead>
                                <tr>
                                    <th>Fecha</th>
                                    <th>Moneda comprada</th>                        
                                    <th>Monto comprado</th>
                                    <th>Tasa de cambio</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                { 
                                    list.map(item => (
                                        <Transaction 
                                            key={item.id}
                                            item = {item}
                                        />
                                    ))
                                }                    
                            </tbody>
                        </table>
                    </ContainerTable>
            }
            
        </Fragment>
    );
};

Transactions.propTypes = {
    refresh: PropTypes.bool.isRequired
}

export default Transactions;