import React, { useState, useContext, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import Swal from 'sweetalert2';
import PropTypes from 'prop-types';

// importando componente para input currency
import CurrencyInput from '../../helpers/CurrencyInput';

// importando estilos para checkmarks de currency 
import '../../styles/checkmarks.scss';

// importando contexts customs 
import { TransactionContext } from '../../context/TransactionContext';
import { UserContext } from '../../context/UserContext';

import { modalStyles, breaks } from '../../helpers/constants';

const Modal = styled.div`
    ${modalStyles}

    > div {
        background-color: #FFF;
        border-radius: 5px;
        border: 1px solid rgba(0,0,0,.2);
        width: 500px;

        header {
            border-bottom: 1px solid rgba(0,0,0,.2);

            h3 {
                color: #a5a5af;
            }

            button {
                position: absolute;
                right: 0;
                top: 0;
                color: #a5a5af;
                font-weight: 700;
                background: transparent;
                border: none;
            }
        }
        
    }

    ${breaks} {
        > div {
            width: 90%;
        }
    }
`;

const styleBaseText = `    
    position: absolute;
    top: 50%;
    right: 20px;
    z-index: 1;
`;

const FormRequest = styled.form`
    color: #57bc90;
    flex-wrap: wrap;

    input {
        color: #85858f;
    }

    input:disabled {
        background-color: black;
    }

    .BTC_TEXT {
        ::before {
            ${styleBaseText}
            content: 'BTC';
        }
    }

    .USD_TEXT {
        ::before {
            ${styleBaseText}
            content: 'USD';
        }
    }

    .container-currency {
        width: calc(100% - 32px);
        height: 30px;
    }

    ${breaks} {
        .container-currency {
            width: calc(100% - 60px);
            height: 30px;
        }
    }
`;

const ModalRequest = ({action, exchange, userLogged, isRefresh, history}) => {

    const [data, setData] = useState({
        currency_send: 'USD',
        currency_receiver: 'BTC'
    });

    const [amountError, setAmountError] = useState(false);
    const [received, setReceived] = useState("0,00000000");
    const [sended, setSended] = useState("0,00");

    const { createTransaction } = useContext(TransactionContext);
    const { updateSession } = useContext(UserContext);

    const calculateReceived = (value) => {
        let amount;

        value = value.replace(/\./gi,'');
        value = parseFloat(value.replace(/\,/gi,'.'));
        
        if(data.currency_send === 'USD'){

            if(userLogged.balance_USD < value)
                setAmountError(true);
            else
                setAmountError(false);

            amount = (value/exchange.rate_sell).toFixed(8);
        } else {

            if(userLogged.balance_BTC < value)
                setAmountError(true);
            else
                setAmountError(false);

            amount = (value*exchange.rate_buy).toFixed(2);
        }

        setReceived(amount.replace('.',','));
    }

    const actionsSubmit = () => {
        updateSession();
        
        if(history.location.pathname.indexOf("dashboard") < 1)
            history.push('/dashboard');
        else {
            isRefresh(true);
        }
    }

    function getAmountDefault(currency) {
        return currency === 'USD' ? { received: "0,00000000", sended: "0,00" } : { received: "0,00", sended: "0,00000000" } 
    }

    const handleChangeCurrency = e => {

        setReceived( getAmountDefault(e.target.value).received );
        setSended( getAmountDefault(e.target.value).sended );

        document.getElementById('amount_send').value = "";

        setData({
            currency_send: e.target.value,
            currency_receiver: e.target.value === 'USD' ? 'BTC' : 'USD'
        })
    }

    const handleChangeAmount = e => {
        if(e.target.value === ""){
            setSended(getAmountDefault(data.currency_send).sended);
            setReceived(getAmountDefault(data.currency_send).received)
        } else {
            setSended(e.target.value)
            calculateReceived(e.target.value);
        }
    }

    const handleSubmit = e => {
        e.preventDefault();
        
        let dataSend = {...data};
        dataSend.amount_send = sended;

        createTransaction(dataSend).then(
            response => {
                if(response.status === 'success')
                    Swal.fire({
                        title: '¡Exito!',
                        text: response.message,
                        icon: 'success',
                        showConfirmButton: false,
                        timerProgressBar: true,
                        timer: 2500
                    })
                    .then(() => {
                        actionsSubmit();
                    });
                else
                    Swal.fire({
                        title: '¡Error!',
                        text: response.message,
                        icon: 'error',
                        showConfirmButton: false,
                        timerProgressBar: true,
                        timer: 2500
                    })
                    .then(() => {
                        actionsSubmit();
                    });
                action(false);
            }
        );
    }

    useEffect(() => {
        calculateReceived(sended);
    },[exchange]);

    return (
        <Modal>
            <div
                className="p-2"
            >
                <header
                    className="pb-1"
                >
                    <h3
                        className="my-0 text-center"
                    >Nueva solicitud</h3>
                    <button
                        type="button"
                        onClick={() => action(false)}
                    >
                        X
                    </button>
                </header>
                <section>
                    <FormRequest
                        id="form_transaction"
                        className="d-flex py-2"
                        onSubmit={handleSubmit}
                    >
                        <div
                            className="col-12 col-md-6"
                        >
                            <h3
                                className="mb-0 mt-0 text-semibold"
                            >Tengo</h3>
                            <div 
                                className="d-flex align-items-center justify-content-between mt-1 container-currency"
                            >
                                <label 
                                    className="slide-radio"
                                > USD
                                    <input 
                                        type="radio"
                                        name="currency_send"
                                        value="USD"
                                        onChange={handleChangeCurrency}
                                        checked={data.currency_send === 'USD'}
                                    /> 
                                    <span className="slide-checkmark"></span> 
                                </label>
                                <label 
                                    className="slide-radio"
                                > BTC
                                    <input 
                                        type="radio"
                                        value="BTC"
                                        name="currency_send"
                                        onChange={handleChangeCurrency}
                                        checked={data.currency_send === 'BTC'}
                                    /> 
                                    <span className="slide-checkmark"></span> 
                                </label>
                            </div>
                        </div>
                        <div className="row mt-1">
                            <div 
                                className={`col-12 col-md-6 ${data.currency_send === 'USD' ? 'USD_TEXT' : 'BTC_TEXT'}`}
                            >
                                <label 
                                    htmlFor="amount_send"
                                    className="text-semibold mt-1 d-block"
                                >Cantidad a enviar:</label>
                                <CurrencyInput 
                                    type="text"
                                    name="amount_send"
                                    id="amount_send"
                                    onChange={ handleChangeAmount }
                                    className="form-control mt-1"
                                    placeholder={`${sended}`}
                                    required
                                    decimals={data.currency_send === 'USD' ? '2' : '8'}
                                />
                                <small
                                    className="d-block mt-1"
                                >
                                    { exchange ? 
                                        data.currency_send === 'USD' ?
                                            `1BTC = ${exchange.rate_sell.toFixed(2)}USD` 
                                            :
                                            `${exchange.rate_buy.toFixed(2)}USD = 1BTC`
                                        :
                                        null
                                    }
                                </small>
                            </div>
                            <div 
                                className={`col-12 col-md-6 ${data.currency_send === 'BTC' ? 'USD_TEXT' : 'BTC_TEXT'}`}
                            >
                                <label 
                                    htmlFor="amount_received"
                                    className="text-semibold mt-1 d-block"
                                >Cantidad a recibir:</label>
                                <CurrencyInput 
                                    type="text"
                                    name="amount_received"
                                    id="amount_received"
                                    value={ received }
                                    className="form-control mt-1"
                                    placeholder={received}
                                    readOnly
                                    required
                                    decimals={data.currency_receiver === 'USD' ? '2' : '8'}
                                />
                            </div>
                        </div>
                        <div className="col-12 mt-1">
                            <p className="d-block my-1 py-0">
                                Al enviar aceptas haber leido y aceptado nuestros <a href="!#">Terminos y Condiciones</a>
                            </p>
                            { amountError ? 
                                <small
                                    className="d-block text-error"
                                >
                                    Lo sentidos no tienes <b>{data.currency_send}</b> suficientes
                                </small>
                                : null
                            }
                            
                            <button 
                                type="submit"
                                className="btn btn-primary mt-1 w-100"
                                disabled={amountError}
                            >
                                Enviar
                            </button>
                        </div>
                    </FormRequest>
                </section>
            </div>
        </Modal>
    );
};

ModalRequest.propTypes = {
    history: PropTypes.object.isRequired,
    exchange: PropTypes.object.isRequired,
    userLogged: PropTypes.object.isRequired,
    isRefresh: PropTypes.func,
    action: PropTypes.func.isRequired
}

export default withRouter(ModalRequest);