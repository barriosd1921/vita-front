import React, { useState, useContext } from 'react';
import { withRouter, Link } from 'react-router-dom';
import Swal from 'sweetalert2';

import { UserContext } from '../../context/UserContext';

import styled from 'styled-components';
import BG_MV from '../../assets/BG_MV.jpg';
import BG_WB from '../../assets/BG_WB.jpg';

import { breaks } from '../../helpers/constants';

const Container = styled.div`
    background: url(${BG_WB}) no-repeat top left/cover;
    height: calc(100vh - 100px);

    ::before {
        content: '\\A';
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background-color: rgba(87, 188, 144,.5);
        z-index: 0;
    }

    ${breaks} {
        background: url(${BG_MV}) no-repeat top left/cover;
    }
`;

const Box = styled.div`
    background-color: #FFF;
    position: absolute;
    right: 50px;
    top: 50%;
    transform: translate(0,-50%);
    border-radius: 5px;
    box-shadow: -2px 2px 4px #77c9d4;
    width: 450px;

    h1 {
        color: #a5a5af;
    }

    ${breaks} {
        width: 90%;
        right: 50%;
        transform: translate(50%,-50%);
    }
`;

const Form = styled.form`
    margin: 20px;
    
    input {
        margin-bottom: 10px;
    }

    a {
        color: #a5a5af;
        text-decoration: none;
    }

    button {
        display: block;
        width: -webkit-fill-available;
    }
`;

const Register = ({history}) => {

    // Creando state local para login 
    const [data, setData] = useState({
        name: '',
        surname: '',
        email: '',
        password: '',
        repassword: ''
    });

    // Exportando context de user para login 
    const { createUser } = useContext(UserContext);

    const handleInput = e => {
        setData({
            ...data,
            [e.target.name] : e.target.value
        });
    }

    const handleSubmit = e => {
        e.preventDefault();

        createUser(data)
            .then( response => {
                    if(response.status === 'success'){
                        Swal.fire({
                            title: '¡Bienvenido!',
                            text: 'Inicia sesion con tu nueva cuenta y disfruta VITA TEST',
                            icon: 'success',
                            showConfirmButton: false,
                            timerProgressBar: true,
                            timer: 3000
                        })
                        .then( () => {
                            history.push('/login');
                        });
                    } else {
                        console.error(response);
                    }
                }
            )
            .catch( e => {
                console.error(e);
                Swal.fire({
                    title: 'Lo Sentimos',
                    text: 'Es posible que tu correo electrónico ya este registrado',
                    icon: 'error',
                    showConfirmButton: false,
                    timerProgressBar: true,
                    timer: 3000
                });
            });
    }

    return (
        <Container>
            <Box>
                <h1
                    className="text-center my-1"
                >Registro de usuario</h1>
                <Form
                    onSubmit={handleSubmit}
                >
                    <input 
                        type="text"
                        name="name"
                        className="form-control"
                        value={data.name}
                        onChange={handleInput}
                        required
                        placeholder="Nombre"
                    />
                    <input 
                        type="text"
                        name="surname"
                        className="form-control"
                        value={data.surname}
                        onChange={handleInput}
                        required
                        placeholder="Apellido"
                    />
                    <input 
                        type="email"
                        name="email"
                        className="form-control"
                        value={data.email}
                        onChange={handleInput}
                        required
                        placeholder="Correo electrónico"
                    />
                    <input 
                        type="password" 
                        name="password"
                        className="form-control"
                        value={data.password}
                        onChange={handleInput}
                        required
                        placeholder="Contraseña"
                    />
                    <input 
                        type="password" 
                        name="repassword"
                        className="form-control"
                        value={data.repassword}
                        onChange={handleInput}
                        required
                        placeholder="Repita contraseña"
                    />
                    {
                        data.password !== data.repassword ? 
                            <small className="d-block mt-1 text-error">
                                Las contraseñas no son iguales
                            </small>
                            :
                            null
                    }
                    
                    <Link 
                        to="/login"
                        className="my-2 d-block"
                    >¿Ya tienes cuenta? Inicia sesión</Link>
                    <button
                        type="submit"
                        className="btn btn-primary mt-1"
                        disabled={data.password !== data.repassword}
                    >Registrarme</button>
                </Form>
            </Box>
        </Container>
    );
};

export default withRouter(Register);